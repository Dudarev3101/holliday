<?php

declare(strict_types=1);

use App\Application\Actions\Controllers\ExcelParser;
use App\Application\Actions\Controllers\Sender;
use App\Application\Actions\Controllers\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });
        $app->get('/ExcelParser', ExcelParser::class);
        $app->get('/Check', Sender::class);

};
