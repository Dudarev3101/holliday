<?php

declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use Slim\App;
use Slim\Views\Twig;

return function (App $app) {
    $app->add(SessionMiddleware::class);
    $app->add(\Slim\Views\TwigMiddleware::createFromContainer($app, Twig::class));
};
