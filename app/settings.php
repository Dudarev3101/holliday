<?php

declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;
use RedBeanPHP\R as R;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            $settings = new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => true,
                'logErrorDetails'     => true,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'database' => [
                    'host' => 'localhost',
                    'base' => 'holidays_db',
                    'user' => 'root',
                    'pass' => ''
                ],

            ]);
            R::setup( 'mysql:host=localhost;dbname=holidays_db',
                'root', '' ); //for both mysql or mariaDB
            \App\Application\Actions\DBConn::Connection($settings);
            return $settings;
        }
    ]);
};
