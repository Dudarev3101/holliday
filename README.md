# Slim Framework 4 Whether Application

## Install the Application

Run this command from the directory in which you want to install your new Slim Framework application. You will require PHP 7.4 or newer.

```bash
composer update && composer start
```

That's it! Now go build something cool.
