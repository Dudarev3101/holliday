<?php

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Slim\Views\Twig;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


abstract class VisualAction extends Action
{
    protected Twig $view;

    public function __construct(LoggerInterface $logger, Twig $view)
    {
        parent::__construct($logger);
        $this->view = $view;
    }

    /**
     * @param string $template наименование шаблона для рендера
     * @param mixed $data данные для рендера
     * @return ResponseInterface
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function generatePage(string $template, $data = [])
    {
        return $this->view->render($this->response, $template, $data);
    }

    protected function generateStatusPage($template, $data = [], $status = 200)
    {
        return $this->generatePage($template, $data)->withStatus($status);
    }
}
