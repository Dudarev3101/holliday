<?php

namespace App\Application\Actions;

use ORM;
use PDO;

class DBConn
{
    public static function Connection($settings){

        $dbc = $settings->get('database');

        ORM::configure('mysql:host=localhost;dbname=holidays_db');
        ORM::configure('username', 'root');
        ORM::configure('password', '');
        ORM::configure('return_result_sets', true);
        ORM::configure('driver_options', array(1002 => 'SET NAMES utf8mb4'));
        ORM::configure('error_mode', PDO::ERRMODE_WARNING);
    }
}
