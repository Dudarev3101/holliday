<?php

namespace App\Application\Actions\Controllers\PeopleColors;

class Former
{
    public function process()
    {
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load('../public/test.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();
        for ($row = 28; $row <= $highestRow; $row++) {
            for ($col = 9; $col <= 9; $col++) {
                $value = $worksheet->getCellByColumnAndRow($col+1, $row)->getFormattedValue();
                $color = $worksheet->getCellByColumnAndRow($col, $row)->getstyle()->getFill()->getStartColor()->getARGB();
                $setups[] = [
                    "name" => $value,
                    "color" => substr($color, 2)
                ];// первые 2 символа указывают на прозрачность фона ячейки
            }
        }
        foreach ($setups as $value){
            $Peoples[] = new People($value["name"], "", $value["color"]);
        }
        return $Peoples;
    }
}
