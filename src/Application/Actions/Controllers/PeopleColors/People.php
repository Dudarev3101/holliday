<?php

namespace App\Application\Actions\Controllers\PeopleColors;

class People extends Former
{
    Public string $name;
    Public string $email;
    Public string $hex_color;

    public function __construct($name, $email, $color){

         return [
             $this->name=$name,
             $this->email=$email,
             $this->hex_color=$color
         ];
    }

}
