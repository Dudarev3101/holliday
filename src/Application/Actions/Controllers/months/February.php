<?php

namespace App\Application\Actions\Controllers\months;

class February
{
    public int $max_column = 15;
    public int $min_column = 9;
    public int $max_row = 10;
    public int $min_row = 3;
}
