<?php

namespace App\Application\Actions\Controllers\months;

class June
{
    public int $max_column = 15;
    public int $min_column = 9;
    public int $max_row = 18;
    public int $min_row = 11;
}
