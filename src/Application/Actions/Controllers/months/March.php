<?php

namespace App\Application\Actions\Controllers\months;

class March
{
    public int $max_column = 22;
    public int $min_column = 16;
    public int $max_row = 10;
    public int $min_row = 3;
}
