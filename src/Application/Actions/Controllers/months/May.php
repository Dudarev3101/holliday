<?php

namespace App\Application\Actions\Controllers\months;

class May
{
    public int $max_column = 8;
    public int $min_column = 2;
    public int $max_row = 18;
    public int $min_row = 11;
}
