<?php

namespace App\Application\Actions\Controllers\months;

class November
{
    public int $max_column = 22;
    public int $min_column = 16;
    public int $max_row = 26;
    public int $min_row = 19;
}
