<?php

namespace App\Application\Actions\Controllers\months;

class January
{
    public int $max_column = 8;
    public int $min_column = 2;
    public int $max_row = 10;
    public int $min_row = 3;
}
