<?php

namespace App\Application\Actions\Controllers\months;

class September
{
    public int $max_column = 8;
    public int $min_column = 2;
    public int $max_row = 26;
    public int $min_row = 19;
}
