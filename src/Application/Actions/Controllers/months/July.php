<?php

namespace App\Application\Actions\Controllers\months;

class July
{
    public int $max_column = 22;
    public int $min_column = 16;
    public int $max_row = 18;
    public int $min_row = 11;
}
