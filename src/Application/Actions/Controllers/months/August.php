<?php

namespace App\Application\Actions\Controllers\months;

class August
{
    public int $max_column = 29;
    public int $min_column = 23;
    public int $max_row = 18;
    public int $min_row = 11;
}
