<?php

namespace App\Application\Actions\Controllers\months;

class April
{
    public int $max_column = 29;
    public int $min_column = 23;
    public int $max_row = 10;
    public int $min_row = 3;
}
