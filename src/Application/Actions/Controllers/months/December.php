<?php

namespace App\Application\Actions\Controllers\months;

class December
{
    public int $max_column = 29;
    public int $min_column = 23;
    public int $max_row = 26;
    public int $min_row = 19;
}
