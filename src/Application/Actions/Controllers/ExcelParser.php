<?php
declare(strict_types=1);

namespace App\Application\Actions\Controllers;

use App\Application\Actions\Controllers\months\April;
use App\Application\Actions\Controllers\months\August;
use App\Application\Actions\Controllers\months\December;
use App\Application\Actions\Controllers\months\February;
use App\Application\Actions\Controllers\months\January;
use App\Application\Actions\Controllers\months\July;
use App\Application\Actions\Controllers\months\June;
use App\Application\Actions\Controllers\months\March;
use App\Application\Actions\Controllers\months\May;
use App\Application\Actions\Controllers\months\November;
use App\Application\Actions\Controllers\months\October;
use App\Application\Actions\Controllers\months\September;
use App\Application\Actions\Controllers\PeopleColors\Colors;
use App\Application\Actions\Controllers\PeopleColors\Former;
use App\Application\Actions\Controllers\PeopleColors\People;
use App\Application\Actions\Tools;
use App\Application\Actions\VisualAction;
use Psr\Http\Message\ResponseInterface as Response;

class ExcelParser extends VisualAction
 {
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(false);
        $spreadsheet = $reader->load('../public/test.xlsx');
        $worksheet = $spreadsheet->getActiveSheet();
        $highestRow = $worksheet->getHighestRow();

        $array = [
            "Январь" => new January(),
            "Февраль" => new February(),
            "Март" => new March(),
            "Апрель" => new April(),
            "Май" => new May(),
            "Июнь" => new June(),
            "Июль" => new July(),
            "Август" => new August(),
            "Сентябрь" => new September(),
            "Октябрь" => new October(),
            "Ноябрь" => new November(),
            "Декабрь" => new December()
            ];
        foreach ($array as $key => $value) {
            for ($row = $array[$key]->min_row; $row <= $array[$key]->max_row; $row++) {
                for ($col = $array[$key]->min_column; $col <= $array[$key]->max_column; $col++) {
                    $value = $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
                    $color = $worksheet->getCellByColumnAndRow($col, $row)->getstyle()->getFill()->getStartColor()->getARGB();
                    if($color != "FFFFFFFF")
                    $result[$key][] = [
                        "дата" => $value,
                        "color" => substr($color, 2)
                    ]; // первые 2 символа указывают на прозрачность фона ячейки
                }
            }
        }

        /* FOR PEOPLES HEX COLORS */
        $Temporary =  (new Former())->process();

        (new DB_init)->Create_empty_months(); //* UPDATE MONTHS TABLE *//
        (new DB_init)->Create_users_table($Temporary); //* UPDATE USERS TABLE *//

        foreach ($result as $month_name => $days) {
            foreach ($days as $day) {
                foreach ($Temporary as $people => $params) {
                    if ($day["color"] == $params->hex_color) {
                        $params->dates[$month_name][] = $day["дата"];
                    }
                    $Big_data[$params->id] = $params;
                }
            }
        }

        return $this->respondWithData(["body"=>(new DB_init)->Render_main_table($Big_data)], 200);

        return $this->respondWithData(["body"=>$Temporary], 200);
        //print_r(Tools::vardump($Temporary));
        //return $this->generatePage("hello.twig", array());
    }
}
