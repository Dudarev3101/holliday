<?php
declare(strict_types=1);

namespace App\Application\Actions\Controllers;

use App\Application\Actions\Action;
use App\Application\Actions\Controllers\PeopleColors\Colors;
use Psr\Http\Message\ResponseInterface as Response;
use RedBeanPHP\R as R;

class Sender extends Action
 {
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $aRows = R::getAll("SELECT * FROM holi");

        return $this->respondWithData(["body"=>$aRows], 200);
    }
}
