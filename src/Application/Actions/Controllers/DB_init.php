<?php
declare(strict_types=1);

namespace App\Application\Actions\Controllers;

use App\Application\Actions\Controllers\PeopleColors\Colors;
use \RedBeanPHP\R as R;
use RedBeanPHP\RedException\SQL;
use function DI\string;

class DB_init
 {
    public array $months;

    public function __construct(){
        $this->months = [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ];
    }

    public function Create_empty_months()
    {
        $saver = false;
        foreach ($this->months as $k => $month) {
            $bean = R::load("months", $k+1);
            $bean->name = $month;
            $saver = R::store($bean);
        }
        return $saver; //id of last month returned
    }

    public function Create_users_table($users){
        $saver = false;
        foreach($users as $k => $user){
            $bean = R::load("users", $k+1);
            $bean->name = $user->name;
            $bean->hex_color = $user->hex_color;
            $bean->email = $user->email;
            $saver = R::store($bean);
            $user->id = $saver;
        }
        return $saver; //id of last month returned
    }

    public function Render_main_table($bid_data){
        R::wipe( 'holi');
        $aRows = R::getAll("SELECT * FROM months");
        foreach($bid_data as $key => $value){

            if(isset($value->dates)) {
                foreach ($value->dates as $month => $dates) {
                    foreach($aRows as $key => $name){
                        if($name["name"] == $month) {
                            $max = count($dates);
                           // return $max;
                            $migrate = R::dispense('holi');
                            $migrate->user_id = $value->id;
                            $migrate->user_name = $value->name;
                            $migrate->user_id = $value->id;
                            $migrate->month_id = $name["id"];
                            $migrate->month_name = $name["name"];
                            $migrate->date_from = $dates[0];
                            $migrate->date_to = $dates[$max-1];
                            $id = R::store( $migrate );
                        }
                    }

                }
            }

        }
        return $bid_data;
    }

    public function action($request){
        $Temporary =  (new Colors)->process();
        return $request;
    }
}
