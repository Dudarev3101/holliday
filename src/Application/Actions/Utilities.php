<?php

namespace App\Application\Actions;

use DateTime;
use Slim\Views\Twig;

class Utilities
{
    public static function datesFix(&$string)
    {
            $_monthsList = array(
                "-01-" => "января",
                "-02-" => "февраля",
                "-03-" => "марта",
                "-04-" => "апреля",
                "-05-" => "мая",
                "-06-" => "июня",
                "-07-" => "июля",
                "-08-" => "августа",
                "-09-" => "сентября",
                "-10-" => "октября",
                "-11-" => "ноября",
                "-12-" => "декабря"
            );

            $_mD = date("-m-", strtotime($string)); //для замены
            $string = str_replace($_mD, " ".$_monthsList[$_mD]." ", $string);
            return $string;
    }
}
