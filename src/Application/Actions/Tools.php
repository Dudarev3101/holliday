<?php

namespace App\Application\Actions;

class Tools
{
    public static function vardump($arr, $var_dump = false)
    {
        echo "<pre style='background: #222;color: #54ff00;padding: 20px;'>";
        if ($var_dump) {
            var_dump($arr);
        }
        else {
            print_r($arr);
        }
        echo "</pre>";
    }
    public static function getdayname()
    {
        $n = date("w", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
        $translater =[
            "0" => "Воскресенье",
            "1" => "Понедельник",
            "2" => "Вторник",
            "3" => "Среда",
            "4" => "Четверг",
            "5" => "Пятница",
            "6" => "Суббота"
        ];
        foreach ($translater as $key => $value){
            if($key == $n){
                $n = $value;
            }
        }
        return $n;
    }
}
