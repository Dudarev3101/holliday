<?php
namespace App\TwigExtensions;

use Twig\TwigFilter;

class DebugExtension extends \Slim\Views\TwigExtension
{
    public function getName() : string
    {
        return 'd';
    }

    public function getFilters()
    {
        return [
            new TwigFilter('d', [$this, 'DebugPrint'])
        ];
    }

    public function DebugPrint ($object)
    {
        ob_start();echo '<pre >';
        var_dump($object);
        echo '</pre>';
        return ob_get_clean();
    }
}
